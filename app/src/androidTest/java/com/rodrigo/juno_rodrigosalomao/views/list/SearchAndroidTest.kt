package com.rodrigo.juno_rodrigosalomao.views.list

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.LargeTest
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView.ViewHolder
import androidx.test.rule.ActivityTestRule
import com.rodrigo.juno_rodrigosalomao.R
import org.hamcrest.Matchers.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class SearchAndroidTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(SearchActivity::class.java)

    @Test
    fun searchAndroidTest() {
        val appCompatImageView = onView(allOf(withId(R.id.search_button),isDisplayed()))
        appCompatImageView.perform(click())

        val searchAutoComplete = onView(allOf(withId(R.id.search_src_text),isDisplayed()))

        searchAutoComplete.perform(replaceText("Android"))

        searchAutoComplete.perform(closeSoftKeyboard())

        Thread.sleep(500)

        val recyclerView = onView(allOf(withId(R.id.rvRepos)))
        recyclerView.perform(actionOnItemAtPosition<ViewHolder>(0, click()))

        val textView = onView(allOf(withId(R.id.tvId),isDisplayed()))
        textView.check(matches(withText("12544093")))
    }

}
