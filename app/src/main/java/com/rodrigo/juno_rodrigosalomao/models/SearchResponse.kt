package com.rodrigo.juno_rodrigosalomao.models


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SearchResponse {
    @SerializedName("total_count")
    @Expose
    val totalCount: Int? = null
    @SerializedName("incomplete_results")
    @Expose
    val incompleteResults: Boolean? = null
    @SerializedName("items")
    @Expose
    val searchResults: List<SearchResult>? = null
}
