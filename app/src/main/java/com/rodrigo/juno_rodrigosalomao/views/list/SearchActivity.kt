package com.rodrigo.juno_rodrigosalomao.views.list

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.rodrigo.juno_rodrigosalomao.R
import com.rodrigo.juno_rodrigosalomao.models.SearchResult
import com.rodrigo.juno_rodrigosalomao.repositories.GitHubRepository
import com.rodrigo.juno_rodrigosalomao.views.detail.DetailActivity
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_search.*


class SearchActivity : AppCompatActivity(), SearchContract.View {

    private lateinit var rvAdapter: ResultAdapter
    private lateinit var presenter: SearchPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        val realm: Realm = Realm.getDefaultInstance()!!
        presenter = SearchPresenter(this, GitHubRepository(),realm)

        setUI()

    }

    private fun setUI() {
        setRecycleView()
    }

    var runnable:Runnable?=null
    private val handler = Handler()
    override fun onCreateOptionsMenu(menu: Menu): Boolean {

        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_search, menu)

        val searchItem = menu.findItem(R.id.menu_search)

        var searchView: SearchView? = null
        if (searchItem != null) {
            searchView = searchItem.actionView as SearchView
        }
        searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                presenter.loadRepository(query)
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                if(newText.isNotEmpty() && newText.length > 3){
                    if (runnable!=null)
                        handler.removeCallbacks(runnable)
                    runnable = Runnable { presenter.loadRepository(newText) }
                    handler.postDelayed(runnable, 500)
                }
                return true
            }
        })

        return super.onCreateOptionsMenu(menu)
    }

    private fun setRecycleView() {
        rvRepos.setHasFixedSize(true)
        rvAdapter = ResultAdapter(
            mutableListOf(),
            object : ResultAdapter.OnItemClickListener {
                override fun onItemClick(
                    item: SearchResult,
                    view: View
                ) {
                    val intent = Intent(this@SearchActivity, DetailActivity::class.java)
                    intent.putExtra(ARGS_REPOSITORY,item)
                    startActivity(intent)
                    hideKeyboard(view)
                }
            })
        rvRepos.layoutManager = LinearLayoutManager(this)
        rvRepos.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        rvRepos.adapter = rvAdapter
        rvRepos.addOnScrollListener(object : EndlessRecyclerOnScrollListener() {
            override fun onLoadMore() {
                presenter.loadNextPage()
            }
        })

    }
    fun hideKeyboard(view:View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
        hideKeyboard(progressBar)
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE

    }

    override fun onNewResultLoaded(searchResults: List<SearchResult>) {
        rvAdapter.loadNewResults(searchResults)
    }

    override fun onMoreResultLoaded(searchResults: List<SearchResult>) {
        rvAdapter.updateResults(searchResults)
    }

    override fun showError() {
        Toast.makeText(this, "some error happened", Toast.LENGTH_SHORT).show()
    }

    override fun showError(s: String?) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show()
    }

    companion object {
        const val ARGS_REPOSITORY: String = "argsRepository"
    }
}
