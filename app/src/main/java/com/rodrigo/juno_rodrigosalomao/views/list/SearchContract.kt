package com.rodrigo.juno_rodrigosalomao.views.list

import com.rodrigo.juno_rodrigosalomao.models.SearchResult

interface SearchContract {
    interface View{
        fun onNewResultLoaded(searchResults: List<SearchResult>)

        fun onMoreResultLoaded(searchResults: List<SearchResult>)

        fun showError()

        fun showError(s: String?)

        fun showLoading()

        fun hideLoading()
    }
    interface Presenter{
        fun loadRepository(searchQuery: String)

        fun loadNextPage()

    }
}