package com.rodrigo.juno_rodrigosalomao.views.list

import com.rodrigo.juno_rodrigosalomao.models.SearchResponse
import com.rodrigo.juno_rodrigosalomao.models.SearchResult
import com.rodrigo.juno_rodrigosalomao.repositories.GitHubRepository
import io.realm.Realm
import retrofit2.Response

class SearchPresenter(
    private val mView: SearchContract.View,
    private val mRepository: GitHubRepository,
    private val realm: Realm)
    :SearchContract.Presenter, GitHubRepository.GitHubRepositoryCallback {

    private var sameQuery: Boolean = false
    private var searchQuery:String = ""
    companion object {
        const val NO_DATA = "resolve host"
    }


    override fun loadRepository(searchQuery: String) {
        if(searchQuery.isNotEmpty()){
            mView.showLoading()
            mRepository.searchRepos(searchQuery,this)
        }

        sameQuery = (this.searchQuery == searchQuery)
        this.searchQuery = searchQuery
    }

    override fun loadNextPage() {
        mView.showLoading()
        sameQuery = true
        mRepository.searchNextPage(searchQuery,this)

    }

    override fun handleGitHubError(message: String?, t: Throwable) {
        mView.hideLoading()
        if (t.message?.contains(NO_DATA)!!){
            loadResultsFromRealm()
        }else
            mView.showError(message)
    }

    override fun handleGitHubResponse(response: Response<SearchResponse>?) {
        if (response != null) {
            if (response.isSuccessful) {
                val searchResponse = response.body()
                if (searchResponse != null) {
                    saveOnDataBase(searchResponse)
                    if(sameQuery){
                        mView.onMoreResultLoaded(searchResponse.searchResults!!)
                    }else
                        mView.onNewResultLoaded(searchResponse.searchResults!!)
                } else {
                    mView.showError(response.message())
                }
            } else {
                mView.showError(response.message())
            }
        }
        mView.hideLoading()

    }

    private fun saveOnDataBase(searchResponse: SearchResponse) {
        realm.executeTransaction {
            it.copyToRealmOrUpdate(searchResponse.searchResults)
        }

    }

    private fun loadResultsFromRealm() {

        val results = realm.where(SearchResult::class.java)
            .contains("name",searchQuery)
            .or()
            .contains("fullName",searchQuery)
            .or()
            .contains("description",searchQuery)
            .findAll()

        mView.onNewResultLoaded(realm.copyFromRealm(results))
    }
}