package com.rodrigo.juno_rodrigosalomao.views.list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rodrigo.juno_rodrigosalomao.R
import com.rodrigo.juno_rodrigosalomao.models.SearchResult
import kotlinx.android.synthetic.main.layout_item_repo.view.*

class ResultAdapter(private val items : MutableList<SearchResult>, private val listener: OnItemClickListener) : RecyclerView.Adapter<ResultAdapter.ViewHolder>() {

    interface OnItemClickListener{
        fun onItemClick(item: SearchResult, view: View)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_item_repo, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(items[position],listener)
    }

    fun loadNewResults(searchResults: List<SearchResult>) {
        items.clear()
        items.addAll(searchResults)
        notifyDataSetChanged()
    }
    fun updateResults(searchResults: List<SearchResult>) {
        items.addAll(searchResults)
        notifyDataSetChanged()
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val name = itemView.tvRepoName

        fun bindView(repo: SearchResult,listener: OnItemClickListener) {

            name.text = repo.fullName

            itemView.setOnClickListener{
                listener.onItemClick(repo,itemView)
            }

        }
    }
}
