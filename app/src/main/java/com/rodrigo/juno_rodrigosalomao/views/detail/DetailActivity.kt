package com.rodrigo.juno_rodrigosalomao.views.detail

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.rodrigo.juno_rodrigosalomao.R
import com.rodrigo.juno_rodrigosalomao.R.layout.activity_detail
import com.rodrigo.juno_rodrigosalomao.models.SearchResult
import com.rodrigo.juno_rodrigosalomao.views.list.SearchActivity
import kotlinx.android.synthetic.main.activity_detail.*
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*


class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activity_detail)
        loadData()
        setToolBar()
    }

    private fun setToolBar() {
        supportActionBar?.title = clickedRepository?.name?.capitalize()
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private var clickedRepository: SearchResult? = null

    private fun loadData() {
        clickedRepository = intent.extras.getSerializable(SearchActivity.ARGS_REPOSITORY) as SearchResult?
        if (clickedRepository!=null) {
            tvId.text = clickedRepository!!.id.toString()
            tvName.text = clickedRepository!!.name
            if (clickedRepository!!._private!!)
                tvPrivate.text = getString(R.string.s_yes)
            else
                tvPrivate.text = getString(R.string.s_no)
            tvDesc.text = clickedRepository!!.description
            tviUpdateDate.text = getFormattedDate(clickedRepository!!.updatedAt)
            tvSize.text = getSize(clickedRepository!!)
        }
    }

    private fun getFormattedDate(updatedAt: String?): String {
        val serverFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
        val date = serverFormat.parse(updatedAt)

        val brFormat = SimpleDateFormat("dd/MM/yyyy' as 'HH:mm:ss", Locale.getDefault())
        return brFormat.format(date)

    }

    //thanks from https://stackoverflow.com/a/5599842
    private fun getSize(clickedRepository: SearchResult):String {
        val size = clickedRepository.size?.toDouble()
        if (size==null || size <= 0.0) return "0"
        val units = arrayOf("B", "kB", "MB", "GB", "TB")
        val digitGroups = (Math.log10(size) / Math.log10(1024.0)).toInt()
        return DecimalFormat("#,##0.#").format(
            size / Math.pow(
                1024.0,
                digitGroups.toDouble()
            )
        ) + " " + units[digitGroups]
    }
}
