package com.rodrigo.juno_rodrigosalomao

import android.app.Application
import io.realm.Realm
import io.realm.RealmConfiguration


class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        val realmConfiguration = RealmConfiguration.Builder()
            .deleteRealmIfMigrationNeeded() //DELETE DATA EVERY TIME SCHEMA CHANGES!!!!!
            .build()
        Realm.setDefaultConfiguration(realmConfiguration)

    }
}
