package com.rodrigo.juno_rodrigosalomao.repositories

import android.util.Log
import com.rodrigo.juno_rodrigosalomao.models.SearchResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

open class GitHubRepository {
    var page:Int = 0

    private var gitHubApi: GitHubApi? = RetrofitClient.serverClient.create(GitHubApi::class.java)

    fun searchRepos(query: String,callback: GitHubRepositoryCallback) {
        page = 0
        callSearchAtPage(query, callback)
    }

    fun searchNextPage(query: String,callback: GitHubRepositoryCallback) {
        callSearchAtPage(query, callback)
    }

    private fun callSearchAtPage(
        query: String,
        callback: GitHubRepositoryCallback
    ) {
        val call = gitHubApi?.searchRepos(query, PAGE_SIZE, page)
        call?.enqueue(object : Callback<SearchResponse> {
            override fun onResponse(call: Call<SearchResponse>, response: Response<SearchResponse>) {
                callback.handleGitHubResponse(response)
                if (response.isSuccessful)
                    page++

            }

            override fun onFailure(call: Call<SearchResponse>, t: Throwable) {
                Log.e("GitHubRepository =>", "onFailure", t)
                callback.handleGitHubError(t.message, t)
            }
        })
    }

    interface GitHubRepositoryCallback {
        fun handleGitHubResponse(response: Response<SearchResponse>?)

        fun handleGitHubError(message: String?, t: Throwable)
    }

    companion object {
        const val NO_DATA = 7
        const val PAGE_SIZE = 30

    }
}
