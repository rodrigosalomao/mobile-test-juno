package com.rodrigo.juno_rodrigosalomao.repositories

import com.rodrigo.juno_rodrigosalomao.models.SearchResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface GitHubApi {

    //    https://api.github.com/search/repositories?q=android&per_page=100&page=2
    @Headers("Accept: application/vnd.github.mercy-preview+json")
    @GET("search/repositories")
    fun searchRepos(
        @Query("q") term: String,
        @Query("per_page") perPage: Int,
        @Query("page") page: Int): Call<SearchResponse>
}
